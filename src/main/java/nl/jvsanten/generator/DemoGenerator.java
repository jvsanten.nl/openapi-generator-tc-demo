package nl.jvsanten.generator;

import org.openapitools.codegen.CodegenOperation;
import org.openapitools.codegen.CodegenType;
import org.openapitools.codegen.languages.AbstractJavaCodegen;
import org.openapitools.codegen.model.ModelMap;
import org.openapitools.codegen.model.ModelsMap;
import org.openapitools.codegen.model.OperationMap;
import org.openapitools.codegen.model.OperationsMap;

import java.util.List;
import java.util.Map;

public class DemoGenerator extends AbstractJavaCodegen {

    protected String sourceFolder = "src/main/java";
    protected String apiVersion = "1.0.0";

    public DemoGenerator() {
        super();
        outputFolder = "generated-code/demo";
        apiTemplateFiles.clear();
        modelTemplateFiles.clear();
        apiDocTemplateFiles.clear();
        modelDocTemplateFiles.clear();
        apiTestTemplateFiles.clear();
        modelTestTemplateFiles.clear();

        modelTemplateFiles.put("model.mustache", ".java");
        apiTemplateFiles.put("api.mustache", ".java");
        templateDir = "demo-codegen";
        apiPackage = "nl.jvsanten.demo.api";
        modelPackage = "nl.jvsanten.demo.model";

        apiNameSuffix = "Client";
        modelNameSuffix = "";
    }

    @Override
    public CodegenType getTag() {
        return CodegenType.CLIENT;
    }

    @Override
    public String getName() {
        return "demo-codegen";
    }

    @Override
    public OperationsMap postProcessOperationsWithModels(OperationsMap objs, List<ModelMap> allModels) {
        OperationsMap results = super.postProcessOperationsWithModels(objs, allModels);
        OperationMap ops = results.getOperations();
        List<CodegenOperation> opList = ops.getOperation();
        for (CodegenOperation co : opList) {
            co.httpMethod = co.httpMethod.toLowerCase();
        }
        return results;
    }

    @Override
    public String getHelp() {
        return "Generates a custom API client just for demo purposes";
    }

    @Override
    public ModelsMap postProcessModels(ModelsMap objs) {
        List<Map<String, String>> recursiveImports = objs.getImports();
        recursiveImports.removeIf(map -> map.get("import").contains("io.swagger.annotations"));
        recursiveImports.removeIf(map -> map.get("import").contains("byte[]"));
        return super.postProcessModels(objs);
    }
}
