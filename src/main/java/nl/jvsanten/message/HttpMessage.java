package nl.jvsanten.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Data
@Accessors(chain = true)
public class HttpMessage {

    private String baseUri;
    private String endpoint;
    private ObjectMapper objectMapper = new ObjectMapper();

    public HttpResponse get() {
        try (CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build()) {
            CloseableHttpResponse closeableHttpResponse =
                    closeableHttpClient.execute(
                            new HttpGet(new URI(baseUri + endpoint)));
            return new HttpResponse()
                    .setStatus(closeableHttpResponse.getStatusLine().getStatusCode())
                    .setBody(EntityUtils.toString(closeableHttpResponse.getEntity()));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Unable to execute http request", e);
        }

    }

    public HttpResponse post(Object body) {
        try (CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build()) {
            HttpPost request = new HttpPost(new URI(baseUri + endpoint));
            request.addHeader("Content-Type", "application/json");
            request.setEntity(new StringEntity("details=" + objectMapper.writeValueAsString(body)));
            CloseableHttpResponse closeableHttpResponse =
                    closeableHttpClient.execute(
                            request);
            return new HttpResponse()
                    .setStatus(closeableHttpResponse.getStatusLine().getStatusCode())
                    .setBody(EntityUtils.toString(closeableHttpResponse.getEntity()));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Unable to execute http request", e);
        }
    }

}
