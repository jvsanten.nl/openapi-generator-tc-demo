package nl.jvsanten.message;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class HttpResponse {

    private int status;
    private String body;

}
