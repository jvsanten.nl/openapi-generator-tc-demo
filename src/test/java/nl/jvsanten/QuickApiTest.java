package nl.jvsanten;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import nl.jvsanten.message.HttpMessage;
import nl.jvsanten.message.HttpResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class QuickApiTest {

    @BeforeAll
    public static void setupMock() {
        WireMockServer wireMockServer = new WireMockServer(8085);
        wireMockServer.start();
        WireMock.configureFor(wireMockServer.port());
        stubFor(any(urlEqualTo("/hello")).willReturn(aResponse().withBody("Hello")));
    }

    @Test
    public void quickTest() {
        HttpMessage httpMessage =
                new HttpMessage()
                        .setBaseUri("http://localhost:8085")
                        .setEndpoint("/hello");
        HttpResponse httpResponse = httpMessage.get();
        assertThat(httpResponse.getBody(), equalTo("Hello"));
        assertThat(httpResponse.getStatus(), equalTo(200));
    }

}
