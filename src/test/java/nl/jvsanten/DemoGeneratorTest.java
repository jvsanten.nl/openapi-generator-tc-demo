package nl.jvsanten;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openapitools.codegen.ClientOptInput;
import org.openapitools.codegen.DefaultGenerator;
import org.openapitools.codegen.config.CodegenConfigurator;

import java.io.File;
import java.io.IOException;

public class DemoGeneratorTest {

    @BeforeAll
    public static void cleanup() throws IOException {
        FileUtils.deleteDirectory(new File("target/gen-test"));
    }

    @Test
    public void generatePetStore() {
        final CodegenConfigurator configurator = new CodegenConfigurator()
                .setGeneratorName("demo-codegen")
                .setInputSpec("src/test/resources/openapi/petstore.yaml")
                .setOutputDir("target/gen-test");
        final ClientOptInput clientOptInput = configurator.toClientOptInput();
        DefaultGenerator defaultGenerator = new DefaultGenerator();
        defaultGenerator.opts(clientOptInput).generate();

    }

}
