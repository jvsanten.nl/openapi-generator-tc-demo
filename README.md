# Openapi Generator Demo

This is a quick demo project made for a meetup hosted by TestCoders in Groningen.

All code is shown and provided as is.

License is Apache 2.0 - largely to match the openapi petstore v3.0 yaml used for demonstration purposes.